import time
import math
import random
import operator


# node in the UCT tree
class Node:
    """Node in the tree."""
    def __init__(self, state, action=None, parent=None):
        self.state = state
        self.action = action
        self.parent = parent
        self.Q = 0.
        self.N = 0
        self.children = []
        self.expanded_actions = set()

# constants
Cp = 1/math.sqrt(2)


# functions as found in Browne et al. 2012, alg. 2
def uct_search(s0, t_max):
    if not s0.non_terminal():
        raise ValueError('state must be non terminal')
    t0 = time.perf_counter()
    t_end = t_max + t0
    v0 = Node(s0)
    count = 0
    while time.perf_counter() < t_end:
        vl = tree_policy(v0)
        delta = default_policy(vl.state)
        backup(vl, delta)
        count += 1
    best = best_child(v0, 0.)
    print('{} iterations in {} s.'.format(count, t_max))
    return best.action


def tree_policy(v):
    while v.state.non_terminal():
        if v.state.n_actions() != len(v.children):
            return expand(v)
        else:
            v = best_child(v, Cp)
    return v


def expand(v):
    a = random.choice(list(v.state.actions() - v.expanded_actions))
    v.expanded_actions.add(a)
    vv = Node(v.state.play(a), a, v)
    v.children.append(vv)
    return vv


def best_child(v, c):
    k = c * math.sqrt(2*math.log(v.N))
    return max(((vv.Q/vv.N+k/math.sqrt(vv.N), vv) for vv in v.children),
               key=operator.itemgetter(0))[1]


def default_policy(s):
    while s.non_terminal():
        a = random.choice(list(s.actions()))
        s = s.play(a)
    return s.reward()


def backup(v, delta):
    while v is not None:
        v.N += 1
        v.Q += - delta * v.state.get_player()
        v = v.parent


def backup_negamax(v, delta):
    while v is not None:
        v.N += 1
        v.Q += delta
        delta = - delta
        v = v.parent


# state is supposed to have the following interface
class GenericState:
    def non_terminal(self):
        """False iff the state is terminal (win, lose or draw)."""
        raise NotImplementedError

    def n_actions(self):
        """Number of possible actions."""
        return len(self.actions())

    def actions(self):
        """Set of possible actions."""
        raise NotImplementedError

    def play(self, action):
        """Return a new state after action has been played."""
        raise NotImplementedError

    def reward(self):
        """Return an evaluation of the state (if terminal)."""
        raise NotImplementedError

    def get_player(self):
        """Return the sign of the player."""
