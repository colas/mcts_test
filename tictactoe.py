#! /usr/bin/env python3
import uct
import copy
import sys


class TicTacToeState(uct.GenericState):
    def __init__(self, matrix, player):
        self.player = player
        self.matrix = matrix
        self._actions = self.gen_actions()
        self.check_terminal()

    def gen_actions(self):
        """List all possible actions."""
        actions = set()
        for i in range(3):
            for j in range(3):
                if self.matrix[i][j] == 0:
                    actions.add((i, j))
        return actions

    def check_terminal(self):
        """Check whether terminal or not."""
        self._terminal = False
        for i in range(3):
            # line victory
            if abs(sum(self.matrix[i][j] for j in range(3))) == 3:
                self._terminal = True
                self._reward = sum(self.matrix[i][j] for j in range(3))/3
                return
            # column victory
            if abs(sum(self.matrix[j][i] for j in range(3))) == 3:
                self._terminal = True
                self._reward = sum(self.matrix[j][i] for j in range(3))/3
                return
        # first diagonal victory
        if abs(sum(self.matrix[j][j] for j in range(3))) == 3:
            self._terminal = True
            self._reward = sum(self.matrix[j][j] for j in range(3))/3
        # second diagonal victory
        elif abs(sum(self.matrix[j][2-j] for j in range(3))) == 3:
            self._terminal = True
            self._reward = sum(self.matrix[j][2-j] for j in range(3))/3
        # no more actions
        if (not self._terminal) and (not len(self.actions())):
            self._terminal = True
            self._reward = 0

    def play(self, action):
        i, j = action
        new_matrix = copy.deepcopy(self.matrix)
        new_matrix[i][j] = self.player
        return TicTacToeState(new_matrix, -self.player)

    def non_terminal(self):
        return not self._terminal

    def n_actions(self):
        return len(self._actions)

    def actions(self):
        return self._actions

    def reward(self):
        if self._terminal:
            return self._reward
        else:
            raise ValueError('not a terminal state.')

    def get_player(self):
        return self.player

    def __str__(self):
        """Print current state."""
        glyph = {-1: 'X', 1: 'O', 0: '.'}
        return '\n'.join(''.join(glyph[self.matrix[i][j]] for j in range(3))
                         for i in range(3))


def ia_move(state, t_max):
    a = uct.uct_search(state, t_max)
    print(a)
    return a


def player_move(state):
    while True:
        s = input('Enter move "row column" starting from top left: ')
        try:
            i, j = [int(c.strip()) for c in s.split()]
            if state.matrix[i][j] == 0:
                return (i, j)
        except ValueError:
            pass


def main():
    init_matrix = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    init_player = 1
    init_state = TicTacToeState(init_matrix, init_player)
    current_state = init_state
    if len(sys.argv) > 1:
        human_player = int(sys.argv[1])
    else:
        human_player = 1
    if len(sys.argv) > 2:
        t_max = float(sys.argv[2])
    else:
        t_max = 0.5
    while current_state.non_terminal():
        print(current_state)
        if current_state.player == human_player:
            a = player_move(current_state)
        else:
            a = ia_move(current_state, t_max)
        current_state = current_state.play(a)
    print(current_state)
    r = current_state.reward()
    if r == 0:
        print('Draw (as expected).')
    elif r == human_player:
        print('Human won (there must be a bug or no time enough to think).')
    else:
        print('Computer won (but you made it on purpose, right?).')


if __name__ == '__main__':
    main()
