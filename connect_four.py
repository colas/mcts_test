#! /usr/bin/env python3
import uct
import copy
import sys


class ConnectFourState(uct.GenericState):
    def __init__(self, matrix, player):
        self.player = player
        self.matrix = matrix
        self._actions = self.gen_actions()
        self.check_terminal()

    def gen_actions(self):
        """List all possible actions."""
        actions = set()
        for j in range(7):
            if self.matrix[5][j] == 0:
                actions.add(j)
        return actions

    def check_terminal(self):
        """Check whether terminal or not."""
        i_coords = range(6)
        j_coords = range(7)
        for i in i_coords:
            for j in j_coords:
                c = self.matrix[i][j]
                if c == 0:
                    continue
                for dir_i, dir_j in ((0, 1), (1, 1), (1, 0), (1, -1)):
                    for k in range(1, 4):
                        if ((i+k*dir_i) not in i_coords) or\
                                ((j+k*dir_j) not in j_coords) or\
                                self.matrix[i+k*dir_i][j+k*dir_j] != c:
                            break
                    else:
                        self._terminal = True
                        self._reward = c
                        return
        if not self._actions:
            # full
            self._terminal = True
            self._reward = 0
        else:
            self._terminal = False

    def play(self, action):
        j = action
        for i in range(6):
            if self.matrix[i][j] == 0:
                break
        else:
            raise ValueError('column %d is full.' % (j+1))
        new_matrix = copy.deepcopy(self.matrix)
        new_matrix[i][j] = self.player
        return ConnectFourState(new_matrix, -self.player)

    def non_terminal(self):
        return not self._terminal

    def n_actions(self):
        return len(self._actions)

    def actions(self):
        return self._actions

    def reward(self):
        if self._terminal:
            return self._reward
        else:
            raise ValueError('not a terminal state.')

    def get_player(self):
        return self.player

    def __str__(self):
        """Print current state."""
        glyph = {-1: 'X', 1: 'O', 0: '.'}
        return '\n'.join(' '.join(glyph[self.matrix[i][j]] for j in range(7))
                         for i in range(5, -1, -1))


def ia_move(state, t_max):
    a = uct.uct_search(state, t_max)
    print(a+1)
    return a


def player_move(state):
    while True:
        s = input('Enter move (1..7): ')
        try:
            j = int(s.strip()) - 1
            if j in state.actions():
                return j
        except ValueError:
            pass


def main():
    init_matrix = [[0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0]]
    init_player = 1
    init_state = ConnectFourState(init_matrix, init_player)
    current_state = init_state
    if len(sys.argv) > 1:
        human_player = int(sys.argv[1])
    else:
        human_player = 1
    if len(sys.argv) > 2:
        t_max = float(sys.argv[2])
    else:
        t_max = 0.5
    while current_state.non_terminal():
        print(current_state)
        if current_state.player == human_player:
            a = player_move(current_state)
        else:
            a = ia_move(current_state, t_max)
        current_state = current_state.play(a)
    print(current_state)
    r = current_state.reward()
    if r == 0:
        print('Draw.')
    elif r == human_player:
        print('Human won.')
    else:
        print('Computer won.')


if __name__ == '__main__':
    main()
